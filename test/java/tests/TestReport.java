package tests;

import java.util.ArrayList;
import java.util.Arrays;

public class TestReport {
    public static void failedTest(ArrayList arr, int[] first, int[] second){
        System.out.println("Input: " + arr);
        System.out.println("Your return: " + Arrays.toString(second));
        System.out.println("expected return: " + Arrays.toString(first));
    }
    public static void failedTest(int input, int result, int expected){
        System.out.println("Input: " + input);
        System.out.println("Your return: " + result);
        System.out.println("expected return: " + expected);
    }
    public static void failedTest(int input, double result, double expected){
        System.out.println("Input: " + input);
        System.out.println("Your return: " + result);
        System.out.println("expected return: " + expected);
    }
}
