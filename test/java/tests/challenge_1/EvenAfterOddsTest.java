package tests.challenge_1;

import challenge_1.EvenAfterOdds;
import net.jqwik.api.*;
import tests.TestReport;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

// this test generates over 100 test cases for the problem

class EvenAfterOddsTest {
    private EvenAfterOdds evenAfterOdds = new EvenAfterOdds();

    @Property boolean even_after_odds(@ForAll("int_arrays") ArrayList<Integer> arr){
        Supplier<Stream<Integer>> ints = arr::stream;

        int[] first = Stream.concat(
                ints.get().filter(i -> i % 2 != 0),
                ints.get().filter(i -> i % 2 == 0)
        ).mapToInt(Integer::intValue).toArray();

        int [] second = evenAfterOdds.evenAfterOdds(arr.stream().mapToInt(Integer::intValue).toArray());

        if (first.length != second.length){
            TestReport.failedTest(arr,first,second);
            return false;
        }

        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]){
                TestReport.failedTest(arr,first,second);
                return false;
            }
        }
        return true;

    }

    @Provide
    Arbitrary<ArrayList<Integer>> int_arrays(){
        ArrayList<Integer> ints;
        ArrayList<ArrayList<Integer>> list_of_list_of_ints = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            ints = new ArrayList<>();
            for (int j = 0; j < IntStream.range(0, new Random().nextInt(30)).toArray().length; j++) {
                ints.add(new Random().nextInt(100));
            }
            list_of_list_of_ints.add(ints);
        }
        // add a empty list to test as well
        list_of_list_of_ints.add(new ArrayList<>());

        // add a list of a single int as well
        ArrayList<Integer> single = new ArrayList<>();
        single.add(4);
        list_of_list_of_ints.add(single);

        // add a list that is already ordered as well
        ArrayList<Integer> ordered = new ArrayList<>();
        ordered.add(1);
        ordered.add(3);
        ordered.add(5);
        ordered.add(2);
        ordered.add(4);
        ordered.add(6);

        list_of_list_of_ints.add(ordered);

        return Arbitraries.of(list_of_list_of_ints);
    }
}
