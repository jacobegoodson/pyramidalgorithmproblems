package tests.challenge_1;

import challenge_1.DuplicateNumber;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DuplicateNumberTest {
    @Test
    void duplicateNumberTest1() {
        Assertions.assertEquals(
                0,
                new DuplicateNumber().duplicate(new int[]{0,0})
        );
    }
    @Test
    void duplicateNumberTest2() {
        Assertions.assertEquals(
                3,
                new DuplicateNumber().duplicate(new int[]{0, 2, 3, 1, 4, 5, 3})
        );
    }
    @Test
    void duplicateNumberTest3() {
        Assertions.assertEquals(
                0,
                new DuplicateNumber().duplicate(new int[]{0, 1, 5, 4, 3, 2, 0})
        );
    }
    @Test
    void duplicateNumberTest4() {
        Assertions.assertEquals(
                5,
                new DuplicateNumber().duplicate(new int[]{0, 1, 5, 5, 3, 2, 4})
        );
    }

}
