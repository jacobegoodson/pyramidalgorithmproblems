package tests.challenge_1;

import challenge_1.ReverseAString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ReverseAStringTest {
    private ReverseAString reverseAString = new ReverseAString();

    @Test void reverseAString1(){
        Assertions.assertEquals("", reverseAString.reverse(""));
    }
    @Test void reverseAString2(){
        Assertions.assertEquals("a", reverseAString.reverse("a"));
    }
    @Test void reverseAString3(){
        Assertions.assertEquals("cba", reverseAString.reverse("abc"));
    }
    @Test void reverseAString4(){
        Assertions.assertEquals("olleh", reverseAString.reverse("hello"));
    }
    @Test void reverseAStringRecurse5(){
        Assertions.assertEquals("", reverseAString.reverseAStringRecurse(""));
    }
    @Test void reverseAStringRecurse6(){
        Assertions.assertEquals("a", reverseAString.reverseAStringRecurse("a"));
    }
    @Test void reverseAStringRecurse7(){
        Assertions.assertEquals("cba", reverseAString.reverseAStringRecurse("abc"));
    }
    @Test void reverseAStringRecurse8(){
        Assertions.assertEquals("olleh", reverseAString.reverseAStringRecurse("hello"));
    }



}
