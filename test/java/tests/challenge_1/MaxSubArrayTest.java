package tests.challenge_1;

import challenge_1.MaxSubArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MaxSubArrayTest {
    @Test
    void maxSubArrayTest1() {
        Assertions.assertEquals(
                8,
                new MaxSubArray().maxSubArray(new int[] {1, 2, 3, -4, 6})
        );
    }
    @Test
    void maxSubArrayTest2() {
        Assertions.assertEquals(
                7,
                new MaxSubArray().maxSubArray(new int[] {1, 2, -5, -4, 1, 6})
        );
    }
    @Test
    void maxSubArrayTest3() {
        Assertions.assertEquals(
                18,
                new MaxSubArray().maxSubArray(new int[] {-12, 15, -13, 14, -1, 2, 1, -5, 4})
        );
    }
}
