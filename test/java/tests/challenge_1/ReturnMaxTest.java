package tests.challenge_1;

import challenge_1.ReturnMax;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ReturnMaxTest {
    private ReturnMax returnMax = new ReturnMax();

    @Test
    void testMax1() {
        Assertions.assertEquals(4, returnMax.max(3,4));
    }
    @Test
    void testMax2() {
        Assertions.assertEquals(0, returnMax.max(0,0));
    }
    @Test
    void testMax3() {
        Assertions.assertEquals(-7, returnMax.max(-100,-7));
    }
    @Test
    void testMax4() {
        Assertions.assertEquals(4, returnMax.maxUsingJavaAPI(3,4));
    }
    @Test
    void testMax5() {
        Assertions.assertEquals(0, returnMax.maxUsingJavaAPI(0,0));
    }
    @Test
    void testMax6() {
        Assertions.assertEquals(-7, returnMax.maxUsingJavaAPI(-100,-7));
    }
}
