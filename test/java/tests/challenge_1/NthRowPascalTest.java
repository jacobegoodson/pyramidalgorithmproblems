package tests.challenge_1;

import challenge_1.NthRowPascal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class NthRowPascalTest {
    @Test
    void nthRowTest1() {
        Assertions.assertArrayEquals(
                new int[] {1},
                new NthRowPascal().nthRow(0)
        );
    }
    @Test
    void nthRowTest2() {
        Assertions.assertArrayEquals(
                new int[] {1,1},
                new NthRowPascal().nthRow(1)
        );
    }
    @Test
    void nthRowTest3() {
        Assertions.assertArrayEquals(
                new int[] {1,2,1},
                new NthRowPascal().nthRow(2)
        );
    }
    @Test
    void nthRowTest4() {
        Assertions.assertArrayEquals(
                new int[] {1, 4, 6, 4, 1},
                new NthRowPascal().nthRow(4)
        );
    }
}
