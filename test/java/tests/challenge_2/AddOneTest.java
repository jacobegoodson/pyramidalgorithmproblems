package tests.challenge_2;

import challenge_2.AddOne;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

class AddOneTest {
    private final AddOne addOne = new AddOne();
    @Test
    void testAddOneNum1(){
        Assertions.assertArrayEquals(
                new int[] {1},
                addOne.addOneNumsOnly(new int[] {0}));
    }

    @Test
    void testAddOneNum2() {
        Assertions.assertArrayEquals(
                new int[] {1,2,4},
                addOne.addOneNumsOnly(new int[] {1,2,3})
        );
    }

    @Test
    void testAddOneNum3() {
        Assertions.assertArrayEquals(
                new int[] {1,0,0,0},
                addOne.addOneNumsOnly(new int[] {9,9,9})
        );
    }
    @Test
    void testAddOneString1() {
        Assertions.assertArrayEquals(
                new int[] {1},
                addOne.addOneUsingStrings(new int[] {0}));
    }
    @Test
    void testAddOneString2() {
        Assertions.assertArrayEquals(
                new int[] {1,2,4},
                addOne.addOneUsingStrings(new int[] {1,2,3})
        );
    }
    @Test
    void testAddOneString3() {
        Assertions.assertArrayEquals(
                new int[] {1,0,0,0},
                addOne.addOneUsingStrings(new int[] {9,9,9})
        );
    }
}
