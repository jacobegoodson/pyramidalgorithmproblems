package tests.recursion_tests;

import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;
import recursion.RecursiveFactorial;

import java.util.stream.IntStream;

class FactorialTest {
    private RecursiveFactorial recursiveFactorial = new RecursiveFactorial();
    @Property void factorial_test(@ForAll("ints") int n){
        int user_result = recursiveFactorial.fact(n);
        if (n == 0){
            Assertions.assertEquals(1, user_result, "Input: " + n);
            return;
        }
        Assertions.assertEquals(
                IntStream.range(1, n+1).reduce((n1,n2) -> n1 * n2).getAsInt(),
                user_result,
                "Input: " + n
        );
    }

    @Provide
    Arbitrary<Integer> ints(){
        return Arbitraries.integers().between(0,10);
    }
}
