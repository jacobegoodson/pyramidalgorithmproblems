package tests.recursion_tests;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import org.junit.jupiter.api.Assertions;
import recursion.Palindrome;

class PalindromeTests {
    private Palindrome palindrome = new Palindrome();
    @Property void test_palindrome(@ForAll String s){
        System.out.println(s);
        boolean isPally = new StringBuilder(s).reverse().toString().equals(s);
        Assertions.assertEquals(isPally, palindrome.isPalindrome(s), "Input: " + s);

        // forcing some simple palindrome tests

        isPally = new StringBuilder("madam").reverse().toString().equals("madam");
        Assertions.assertEquals(isPally, palindrome.isPalindrome("madam"), "Input: " + "madam");

        isPally = new StringBuilder("a").reverse().toString().equals("a");
        Assertions.assertEquals(isPally, palindrome.isPalindrome("a"), "Input: " + "a");

        isPally = new StringBuilder("").reverse().toString().equals("");
        Assertions.assertEquals(isPally, palindrome.isPalindrome(""), "Input: " + "");
    }
}
