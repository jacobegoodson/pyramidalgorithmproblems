package tests.recursion_tests;

import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;
import recursion.PowerOf2;

@Label("Testing powerOf2")
class PowerOf2Test {
    private PowerOf2 powerOf2 = new PowerOf2();
    @Property void power_of_2_test(@ForAll("gen_ints") int n){
        double userAnswer = powerOf2.powerOf2(n);
        Assertions.assertEquals(Math.pow(2,n), userAnswer, "Input: " + userAnswer);
    }

    @Provide
    Arbitrary<Integer> gen_ints(){
        return Arbitraries.integers().between(0,100);
    }
}
