package recursion;

// TODO add solution and test cases
/*
Problem Statement
Given an integer array, find and return all the subsets of the array. The order of subsets in the output array is not important. However the order of elements in a particular subset should remain the same as in the input array.

Note: An empty set will be represented by an empty list

Example 1

arr = [9]

output = [[]
          [9]]
Example 2

arr = [9, 12, 15]

output =  [[],
           [15],
           [12],
           [12, 15],
           [9],
           [9, 15],
           [9, 12],
           [9, 12, 15]]
*/

import java.util.ArrayList;

public class Subsets {
    public static ArrayList<ArrayList> subsets(ArrayList arr){
        return innerSubsets(arr, 0);
    }

    private static ArrayList<ArrayList> innerSubsets(ArrayList arr, int i) {
        if (i >= arr.size()){
            return new ArrayList(){{add(new ArrayList());}};
        }

        ArrayList<ArrayList> smallOutput = innerSubsets(arr, i+1);
        ArrayList output = new ArrayList();

        for (Object elm : smallOutput) {
            output.add(elm);
        }
        
        for (ArrayList elm: smallOutput){
            ArrayList current = new ArrayList();
            current.add(arr.get(i));
            current.addAll(elm);
            output.add(current);
        }
        return output;


    }

    public static ArrayList<ArrayList> subset2(ArrayList arr){
        if (arr.isEmpty()){
            return new ArrayList(){{add(new ArrayList());}};
        }
        Object first = arr.get(0);
        ArrayList<ArrayList> rest = subset2(new ArrayList(arr.subList(1, arr.size())));
        ArrayList out = new ArrayList();

        for (ArrayList sub : rest) {
            out.add(sub);
        }

        for (ArrayList sub : rest) {
            ArrayList current = new ArrayList();
            current.add(first);
            current.addAll(sub);
            out.add(current);
        }
        return out;
    }


    public static void main(String[] args) {
        System.out.println(subset2(new ArrayList(){{add(9); add(12); add(15);}}));
    }
}
