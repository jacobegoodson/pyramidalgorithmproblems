package recursion;

/*
Problem Statement
    Define a procedure, deep_reverse, that takes as input a list, and returns a new list that is the deep reverse of the input list.
    This means it reverses all the elements in the list, and if any of those elements are lists themselves, reverses all the elements in the inner list, all the way down.

    Note: The procedure must not change the input list itself.
*/

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class DeepReverse {
    public static ArrayList deepReverse(ArrayList reverseThis){
        return new ArrayList();
    }

    public static void main(String[] args) {
        ArrayList kek = new ArrayList(){{add(1); add(2); add(3);}};
        ArrayList kek4 = new ArrayList(){{add(5); add(6); add(8);}};
        ArrayList kek2 = new ArrayList<>();

        ArrayList nested = new ArrayList(){{add("a"); add("b"); add("c");}};

        kek.add(kek4);

        kek2.add(1);
        kek2.add(1);
        kek2.add(kek);
        kek2.add(nested);
        System.out.println(kek2);
        System.out.println(deepReverse(kek2));
    }
}
