package recursion;

/*
Keypad Combinations
A keypad on a cellphone has alphabets for all numbers between 2 and 9.

You can make different combinations of alphabets by pressing the numbers.

For example, if you press 23, the following combinations are possible:

ad, ae, af, bd, be, bf, cd, ce, cf

Note that because 2 is pressed before 3, the first letter is always an alphabet on the number 2. Likewise, if the user types 32, the order would be

da, db, dc, ea, eb, ec, fa, fb, fc

Given an integer num, find out all the possible strings that can be made using digits of input num. Return these strings in a list. The order of strings in the list does not matter. However, as stated earlier, the order of letters in a particular string matters.
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class KeyPadCombinations {

    static String getCharacters(int num){
        if (num == 2){
            return "abc";
        }
        else if (num == 3){
            return "def";
        }
        else if (num == 4){
            return "ghi";
        }
        else if (num == 5){
            return "jkl";
        }
        else if (num == 6){
            return "mno";
        }
        else if (num == 7){
            return "pqrs";
        }
        else if (num == 8){
            return "tuv";
        }
        else if (num == 9){
            return "wxyz";
        }
        return "";
    }

    static ArrayList<String> keypad(int num){
        if (num <= 1){
            return new ArrayList<>();
        }
        if (num < 9){
            return new ArrayList<>(Arrays.asList(getCharacters(num).split("")));
        }

        String[] lastCode = getCharacters(num % 10).split("");
        ArrayList<String> butLastCode = keypad(num / 10);
        ArrayList<String> ret = new ArrayList<>();
        for(String butLastCodeLetters:butLastCode){
            for (String lastCodeLetter: lastCode){
                ret.add(butLastCodeLetters + lastCodeLetter);
            }
        }

        return ret;
    }

    public static void main(String[] args) {
        System.out.println(23 % 10);
        System.out.println(243 / 10);

        ArrayList<String> sorted = keypad(2346);
        sorted.sort(Comparator.naturalOrder());
        System.out.println(sorted);

    }
}

