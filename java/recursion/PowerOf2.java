package recursion;

/*
Problem Statement
You are given the integer n, return a double that is 2 to the power of n. This calculation should be done recursively.

Example:

int n = 3
int output = 8 (because 8 is 2^3 or 2 to the power of 3)
The expected time complexity for this problem is O(n)
*/

public class PowerOf2 {

    public double powerOf2(int n) {
        // your code goes here
        return 0;
    }
}
