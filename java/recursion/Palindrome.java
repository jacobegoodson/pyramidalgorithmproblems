package recursion;

/*
A palindrome is a word that is the reverse of itself—that is, it is the same word when read forwards and backwards.

For example:

"madam" is a palindrome
"abba" is a palindrome
"cat" is not
"a" is a trivial case of a palindrome
The goal of this exercise is to use recursion to complete the isPalindrome method that takes a string as input and checks whether that string is a palindrome. (Note that this problem can also be solved with a non-recursive solution, but that's not the point of this exercise.)
*/

public class Palindrome {
    public boolean isPalindrome(String s) {
        // Your code goes here
        return false;
    }
}
