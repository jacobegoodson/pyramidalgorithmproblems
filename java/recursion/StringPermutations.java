package recursion;

/*
Problem Statement
Given an input string, return all permutations of the string in an array.

Example 1:

string = "ab"
output = ["ab", "ba"]
Example 2:

string = "abc"
output = ["abc", "bac", "bca", "acb", "cab", "cba"]
*/

import java.util.ArrayList;

public class StringPermutations {
    public static ArrayList<String> stringPermutations(String s){
        return new ArrayList<>();
    }

    public static void main(String[] args) {
        System.out.println(stringPermutations("abc"));
    }
}
