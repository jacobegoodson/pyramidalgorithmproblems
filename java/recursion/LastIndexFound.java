package recursion;

// TODO add solution and tests!

/*
    Problem statement
Given an array arr and a target element target, find the last index of occurence of target in arr using recursion. If target is not present in arr, return -1.

For example:

For arr = [1, 2, 5, 5, 4] and target = 5, output = 3

For arr = [1, 2, 5, 5, 4] and target = 7, output = -1
*/

import java.util.ArrayList;

public class LastIndexFound {
    public static int lastIndexFound(ArrayList<Integer> arr, int target){
        return lastIndexFoundInner(arr, target, arr.size() - 1);
    }
    public static int lastIndexFoundInner(ArrayList<Integer> arr, int target, int index){
        if (index > arr.size()){
            return target;
        }
        int current = arr.get(index);

        if (current == target){
            return target;
        }
        return lastIndexFoundInner(arr,target,index - 1);
    }

    public static void main(String[] args) {
        System.out.println(lastIndexFound(
                new ArrayList<Integer>(){{
                    add(1);
                    add(2);
                    add(3);
                }}, 3

        ));
    }
}
