package recursion;

import java.util.ArrayList;

// TODO test case and solutions

/*

Problem statement
In an encryption system where ASCII lower case letters represent numbers in the pattern a=1, b=2, c=3... and so on, find out all the codes that are possible for a given input number.

Example 1

number = 123
codes_possible = ["aw", "abc", "lc"]
Explanation: The codes are for the following number:

1 . 23 = "aw"
1 . 2 . 3 = "abc"
12 . 3 = "lc"
Example 2

number = 145
codes_possible = ["ade", "ne"]
Return the codes in a list. The order of codes in the list is not important.

Note: you can assume that the input number will not contain any 0s

*/

public class ReturnCodes {
    public static String getAlphabet(int num){
        return String.valueOf((char)(num + 96));
    }
    public static ArrayList<String> returnCodes(int n){
        if (n == 0){
            return new ArrayList(){{add("");}};
        }
        ArrayList<String> out = new ArrayList<>();
        ArrayList<String> letters;
        String currentLetter;
        int rem = n % 100;
        if (rem <= 26 && rem > 9){
             letters = returnCodes(n / 100);
             currentLetter = getAlphabet(rem);
            for (String s : letters) {
                out.add(s + currentLetter);
            }
        }

        rem = n % 10;
        letters = returnCodes(n / 10);
        currentLetter = getAlphabet(rem);

        for (String s : letters) {
            out.add(s + currentLetter);
        }
        return out;
    }

    public static void main(String[] args) {
        System.out.println(returnCodes(123));
    }
}
