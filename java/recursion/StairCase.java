package recursion;

/*
    Problem Statement
Suppose there is a staircase that you can climb in either 1 step, 2 steps, or 3 steps. In how many possible ways can you climb the staircase if the staircase has n steps? Write a recursive function to solve the problem.

Example:

n = 3
output = 4
The output is 4 because there are four ways we can climb the staircase:

1. 1 step +  1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step
4. 3 steps
*/

import sun.font.CreatedFontTracker;

import java.util.HashMap;

public class StairCase {
    static HashMap<Integer,Integer> memo = new HashMap<>();
    public static int stairCase(int n){
        if (n <= 0){
            return 1;
        }
        if (n == 1){
            return 2;
        }
        if (n == 2){
            return 3;
        }
        if (n == 3){
            return 4;
        }

        if (memo.containsKey(n)){
            return memo.get(n);
        } else{
            memo.put(n, stairCase(n-1) + stairCase(n-2) + stairCase(n-3));
        }

        return memo.get(n);
    }

    public static void main(String[] args) {
        System.out.println(stairCase(30));
    }
}
