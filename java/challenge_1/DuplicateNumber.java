package challenge_1;

/*
Problem Statement
You have been given an array of length = n. The array contains integers from 0 to n - 2. Each number in the array is present exactly once except for one number which is present twice. Find and return this duplicate number present in the array

Example:

int[] arr = new int[] {0, 2, 3, 1, 4, 5, 3}
int output = 3 (because 3 is present twice)
The expected time complexity for this problem is O(n)
*/

public class DuplicateNumber {
    public int duplicate(int[] nums) {
        /*your code goes here*/
        return 0;
    }
}
