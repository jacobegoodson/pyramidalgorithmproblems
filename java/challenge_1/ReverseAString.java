package challenge_1;

/*
Problem Statement
You are given a random string of any length, return the string in reverse

Example:

String in = "abc"
int output = "cba"
The expected time complexity for this problem is O(n)

Extra:
Try solving the problem with a recursive solution
Try solving the problem functionally/declarative style in a single line with StringBuilder
*/

public class ReverseAString {
    public String reverse(String stringToReverse){
        // Your code goes here
        return "";
    }

    public String reverseAStringRecurse(String stringToReverse){
        // Your code goes here
        return "";
    }
}
