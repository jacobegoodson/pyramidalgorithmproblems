package challenge_1;

/*
Problem Statement
You have been given an array containing numbers. Find and return the largest sum in a contiguous subarray within the input array.

Example 1:

int [] arr= new int[] {1, 2, 3, -4, 6}
The largest sum is 8, which is the sum of all elements of the array.
Example 2:

int[] arr = new int[] {1, 2, -5, -4, 1, 6}
The largest sum is 7, which is the sum of the last two elements of the array.
*/

public class MaxSubArray {
    public int maxSubArray(int[] arr){
        /*Your code here*/
        return 0;
    }
}
