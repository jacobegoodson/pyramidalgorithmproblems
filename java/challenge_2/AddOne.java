package challenge_2;
/*
Problem Statement
You are given a non-negative number in the form of list elements. For example, the number 123 would be provided as arr = [1, 2, 3]. Add one to the number and return the output in the form of a new list.
Example 1:
•	input = [1, 2, 3]
•	output = [1, 2, 4]
Example 2:
•	input = [9, 9, 9]
•	output = [1, 0, 0, 0]
Challenge:
One way to solve this problem is to convert the input array into a number and then add one to it. For example, if we have input = [1, 2, 3], you could solve this problem by creating the number 123 and then separating the digits of the output number 124.
But can you solve it in some other way?

*/

public class AddOne {
    // implement a method that uses only numbers to solve the problem
    public int[] addOneNumsOnly(int[] nums){
        /*Your code goes here*/

        return new int[] {};
    }

    // implement a method that will add one to the array by first converting it into a string
    public int[] addOneUsingStrings(int[] nums){
        /*Your code goes here*/
        return new int[] {};
    }
}
